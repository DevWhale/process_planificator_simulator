#include "../runqueue/ranqueue.h"
#include <atomic>
#include <thread>

using namespace std;

/*
Hebras E, requisitos:
  Planifica procesos
  Elegir hebra T de la runqueue segun prioridad
  Esperar por tiempo de ejecucion de la hebra T
  Obtener tiempo de computo de la siguiente hebra
  Calcular nueva prioridad
  Obtener Hebra T de la runqueue expirada

*/

struct cpu {
  cpu();
  cpu(const cpu &);
  atomic<runqueue> active;
  atomic<runqueue> expired;
};

class scheduler {
private:
  cpu *runqueues;
  void compute_priority(process *);
  void cpt(process *);
  chrono::duration<int64_t, milli> quantum;

public:
  scheduler(cpu *, const int &);
  void take_active_proc(process *, int &);
  void take_expired_proc(process *, int &);
  void put_in_active(process *);
  void put_in_expire(process *);
  void run();
  void read_active();
  void read_expired();
  void round_robin(double quantum);
  void check_time(process *);
};
