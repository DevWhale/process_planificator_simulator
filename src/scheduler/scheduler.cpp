#include "scheduler.h"

scheduler::scheduler(cpu *runqueues, const int &quantum)
    : runqueues(runqueues) {
  this->quantum = chrono::milliseconds(quantum);
}

void scheduler::compute_priority(process *p) { p->priority++; }

void scheduler::cpt(process *p) { p->time_execution -= quantum; }

void scheduler::take_active_proc(process *p, int &priority) {
  runqueue stack = runqueues->active.load();
  p = stack.get_process(priority);
  runqueues->active.exchange(stack);
}

void scheduler::take_expired_proc(process *p, int &priority) {
  runqueue stack = runqueues->expired.load();
  p = stack.get_process(priority);
  runqueues->expired.exchange(stack);
}

void scheduler::check_time(process *p) {
  if (p->time_execution > 0ms) {
    return;
  }
  auto time_now = chrono::high_resolution_clock::now();
  p->burst_time = time_now - p->arrival_time;
  // TODO print process information at the end el life

  delete p;
}

void scheduler::put_in_active(process *p) {
  check_time(p);
  runqueue stack = runqueues->active.load();
  stack.add_process(p->priority, p);
  runqueues->active.exchange(stack);
}

void scheduler::put_in_expire(process *p) {
  check_time(p);
  runqueue stack = runqueues->expired.load();
  stack.add_process(p->priority, p);
  runqueues->expired.exchange(stack);
}
void scheduler::read_active() {
  process *p = nullptr;
  int priority = 0;
  do {
    // take a process from the runqueue
    take_active_proc(p, priority);
    if (!p) {
      priority++;
      continue;
    }
    // sleep ;
    // recalculate time of the process
    cpt(p);
    // give the process a new priority
    compute_priority(p);
    // put the process in the contrary runqueue
    put_in_expire(p);

  } while (priority <= 9);
}

void scheduler::read_expired() {
  process *p = nullptr;
  int priority = 0;
  do {
    // take a process from the runqueue
    take_expired_proc(p, priority);
    if (!p) {
      priority++;
      continue;
    }
    // sleep a quantum
    this_thread::sleep_for(quantum);
    // recalculate time of the process
    cpt(p);
    // give the process a new priority
    compute_priority(p);
    // put the process in the contrary runqueue
    put_in_active(p);

  } while (priority <= 9);
}

void scheduler::run() {
  // decide the actual runqueue
  // process the active runqueue
  while (runqueues->active.load().get_size()) {
    read_active();
    // process the expired runqueue
    if (!runqueues->expired.load().get_size()) {
      break;
    }
    read_expired();
  }
}
