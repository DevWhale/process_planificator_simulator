#include "../process/process.h"
#include <queue>

using namespace std;

class process_ll {
private:
  int size;
  process *first;
  process *last;

public:
  process_ll();
  process_ll(process *);
  void add_process(process *);
  process *pop_proccess();
  process *get_first_process();
  process *get_last_process();
  int get_size();
};

class runqueue {
private:
  int size;
  process_ll stackl[10];

public:
  runqueue();
  int get_size();
  void add_process(int &, process *);
  process *get_process(int &);
};
