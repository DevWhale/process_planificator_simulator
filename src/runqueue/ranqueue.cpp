#include "ranqueue.h"

process_ll::process_ll() {
  this->first = nullptr;
  this->last = nullptr;
  this->size = 0;
}

process_ll::process_ll(process *first) {
  this->first = first;
  this->last = first;
  this->size = 1;
}

void process_ll::add_process(process *process) {
  this->size++;
  if (this->first == nullptr) {
    this->first = process;
    this->last = process;
    return;
  }
  this->last->set_next_process(process);
  this->last = process;
}
process *process_ll::pop_proccess() {
  process *actual = this->first;
  this->first = actual->get_next_process();
  this->size--;
  return actual;
}

process *process_ll::get_first_process() { return this->first; }
process *process_ll::get_last_process() { return this->last; }
int process_ll::get_size() { return this->size; }

runqueue::runqueue() { this->size = 0; }

int runqueue::get_size() { return this->size; }
void runqueue::add_process(int &p, process *item) {
  this->stackl[p].add_process(item);
  this->size++;
}

process *runqueue::get_process(int &p) {
  this->size--;
  return this->stackl[p].pop_proccess();
}
