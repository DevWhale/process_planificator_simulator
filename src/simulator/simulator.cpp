#include "simulator.h"

simulator::simulator(int n, int m, int p) : n(n), m(m), p(p) {}

void simulator::build(int iter) {
    srand(time(NULL));
    create_HT(n);
    create_HE();
    start_HE();
    double creation_frecuency = 2000;
    int max_creation_iterations = 5;
    int iteration_process_count = 10;
    wait_for_HG(creation_frecuency, max_creation_iterations, iteration_process_count);
  
}

void simulator::create_HT(int nn) {
  for (int i = 0; i < nn; i++) {
    process *temp = new process(def_prio, def_exec_time);
    int id = rand() % 200000;
    runqueues.active.load().add_process(id, temp);
  }
}

void simulator::create_HE() {
  for (int i = 0; i < m; i++) {
    scheduler *temp = new scheduler(&runqueues, def_exec_time);
    threads_HE.push_back(*temp);
  }
}

void* run_HE(void* sh){
  scheduler *temp  = static_cast<scheduler* >(sh);
  (temp->run)();
}

void simulator::start_HE() {
  vector<pthread_t> TH;
  for (int i = 0; i < m; i++) {
    scheduler sch_ptr = threads_HE[i];
    pthread_t temp;
    pthread_create(&temp, NULL, run_HE, &sch_ptr) ;
    TH.push_back(temp);
  }
  for (int i = 0; i < m; i++)
  {
    pthread_join(TH[i],NULL);
  }
  
}

void simulator::wait_for_HG(double incr_frec, int proc_frec, int iter){
  auto start = chrono::high_resolution_clock::now();
  while(iter){
    auto end = chrono::high_resolution_clock::now();
    chrono::duration<double> diff = end - start;
    if(diff.count() > incr_frec){
      create_HT(proc_frec);
      start_HE();
      iter--;
      start = chrono::high_resolution_clock::now();
    }
  }
    
}
