#include "../scheduler/scheduler.h"
#include <iostream>
#include <pthread.h>
#include <vector>

using namespace std;


/*
simulador de procesos, requisitos:
  poseer ambas runqueues
  Crear y mantener hebras T
  Insertar inicialmente las hebras T en las runqueues
  Crear y mantener hebras E
  Entregar runqueues a las hebras E
  Esperar por mas procesos (hebras G)
*/

class simulator {
private:
  cpu runqueues;
  int n; // cantidad de hebras T
  int m; // cantidad de hebras E
  int p; // modo de asignacion de prioridad
  const int def_prio = 2;
  const int def_exec_time = 20;
  vector<scheduler> threads_HE;

public:
  simulator(int, int, int);
  void build(int);
  void create_HT(int);
  void create_HE();
  void start_HE();
  //void run_HE(scheduler);
  void wait_for_HG(double, int, int );
};
