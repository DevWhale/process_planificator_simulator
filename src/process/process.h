#include <chrono>
#include <iostream>

using namespace std;

/*
Hebras T, requisitos:
  capacidad de ser usada sleep()
  tener y modificar prioridad
  tener y modificar quantum
*/

class process {
public:
  int id;
  int priority;
  chrono::duration<int64_t, milli> time_execution;
  chrono::time_point<chrono::high_resolution_clock> arrival_time;
  chrono::duration<double> burst_time;
  process *next;

  process(int);
  process(int, int);
  // PREGUNTA: las siguientes 2 funciones no pertenecen a esta hebra, estas
  // deben estar en las ranqueues
};
