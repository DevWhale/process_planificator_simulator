#include "process.h"

process::process(int execution_time) {
  this->priority = 0;
  this->time_execution = chrono::milliseconds(execution_time);
  this->next = nullptr;
  this->arrival_time = chrono::high_resolution_clock::now();
}
process::process(int priority, int execution_time) {
  if (priority > 9 or priority < 0)
    cout << "Error de valor de prioridad" << endl;
  else
    this->priority = priority;

  this->time_execution = chrono::milliseconds(execution_time);
  this->next = nullptr;
  this->arrival_time = chrono::high_resolution_clock::now();
}
