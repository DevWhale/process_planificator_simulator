#include "src/simulator/simulator.h"
#include <algorithm>
#include <iostream>

using namespace std;

bool exists_option(char **, char **, const string &);
char *read_option_value(char **, char **, const string &);
bool check_arguments(int &argc, char *argv[]);

int main(int argc, char *argv[]) {
  if (!check_arguments(argc, argv)) {
    exit(EXIT_FAILURE);
  }

  // getting option values if they exists
  char *a, *b;
  // -a , -b
  a = read_option_value(argv, argv + argc, "-a");
  b = read_option_value(argv, argv + argc, "-b");

  int min = stoi(a), max = stoi(b);

  if (min > max) {
    cout << "El valor de -a debe ser mayor que el valor de -b" << endl;
    exit(EXIT_FAILURE);
  }
  // printf("[ %i, %i ]\n", min, max);
  // -n , -m
  a = read_option_value(argv, argv + argc, "-n");
  b = read_option_value(argv, argv + argc, "-m");
  int n = stoi(a), m = stoi(b);
  // printf("procesos  = %i\nplanificadores = %i\n", n, m);
  a = read_option_value(argv, argv + argc, "-p");
  int p = stoi(a);
  // printf("algoritmo  = %i\n", alg);
  simulator pc(n, m, p);

  return 0;
}

bool exists_option(char **begin, char **end, const string &opt) {
  return find(begin, end, opt) != end;
}

char *read_option_value(char **begin, char **end, const string &opt) {
  char **it = find(begin, end, opt);
  if (it != end and ++it != end) {
    return *it;
  }
  return 0;
}

bool check_arguments(int &argc, char *argv[]) {
  // see options
  // -a
  if (!exists_option(argv, argv + argc, "-a")) {
    cout << "Debe ingresar el tiempo minino de demora de un proceso de la "
            "forma: -a <tiempo en segundos>"
         << endl;
    return false;
  }
  // -b
  if (!exists_option(argv, argv + argc, "-b")) {
    cout << "Debe ingresar el tiempo maximo de demora de un proceso de la "
            "forma: -b <tiempo en segundos>"
         << endl;
    return false;
  }
  // -n
  if (!exists_option(argv, argv + argc, "-n")) {
    cout << "Debe ingresar el numero  de proceso de la "
            "forma: -n <numero de procesos>"
         << endl;
    return false;
  }
  // -m
  if (!exists_option(argv, argv + argc, "-m")) {
    cout << "Debe ingresar el numero de planificadores de la "
            "forma: -m <numero de planificadores>"
         << endl;
    return false;
  }
  if (!exists_option(argv, argv + argc, "-p")) {
    cout << "Debe ingresar el algoritmo de planificacion a ocupar de la forma"
            "forma: -p <numero de algoritmo>\n"
            "1 -> Shortest job first\n"
            "2 -> First Come First Serve\n"
            "3 -> Round Robin"
         << endl;
    return false;
  }
  return true;
}
